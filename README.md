 * Version 0.9
 
 
 # RoboDash 
 
 RoboDash is a Dashboard application for realtime monitoring of Lego robots. RoboDash uses MQTT for connecting to the gateway server acting as a border router for Bluetooth Lego Robot network. RoboDash is developed as a part of an educational sensor network system project. 
 
 This project involves very little original content as it is mostly just bits and peaces sown together.
 
 ### How do I set up? 
 
* Compile and run in Linux or Windows environment.
 
* VLC player has to be installed.

* Windows 7 requires additional environment variable: VLC_PLUGIN_PATH=<pathToVLCPluginFolder>

* Configure video stream source and MQTT broker address.
 
 ###  Dependencies 
 
 https://github.com/caprica/vlcj 
 
    vlcj-3.10.1.jar
    jna-4.1.0.jar
 
 
 http://xenqtt.sourceforge.net
 
    xenqtt-0.9.6.jar
 
 
 https://github.com/HanSolo/Medusa
 
    Medusa-4.5.jar
 
 
 https://github.com/HanSolo/Colors
 
    colors-1.4.jar
 
 ### MQTT topics 
 
 **To be subscribed to:**
 
 /darkAndCold/temperature
 
 /darkAndCold/brightness
 
 /brightAndCold/temperature
 
 /brightAndCold/brightness
 
 /darkAndWarm/temperature
 
 /darkAndWarm/brightness
 
 /brightAndWarm/temperature
 
 /brightAndWarm/brightness
 
 /robotCoord/Xvalue
 /robotCoord/Yvalue
 /storage/boxPriority
 
 
 **Topics for posting:**
 
 /robotCommand/target
 /robotCommand/boxPriority
 
 
 ### Owner 
 
 * Juho
 
 * ASN Group 3

 
![GUI](img/gui.png)