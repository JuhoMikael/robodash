package com.JuhoMikael.RoboDash;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.StackPane;

public class infoView {
	
	private String valueTemp = "<0-100>";
	private String valueMap = "<0-10>";
	private String priority = "<(0-4)(0-4)(0-4)(0-4)(0-4)>";
	
	public Button exit = new Button("exit");
	
	
	public StackPane getHelp(){
		
		ListView<String> listView = new ListView();
		ArrayList<String> entries;
	    
		String [] entriesS = {"","_____-----RoboDash Beta release-----_____","","","Subscribed MQTT topics:"
				,""
				,"/darkAndCold/temperature/ " + valueTemp
				,"/darkAndCold/brightness/ " + valueTemp
				,"/brightAndCold/temperature/ " + valueTemp
				,"/brightAndCold/brightness/ " + valueTemp
				,"/darkAndWarm/temperature/ " + valueTemp
				,"/darkAndWarm/brightness/ " + valueTemp
				,"/brightAndWarm/temperature/ " + valueTemp
				,"/brightAndWarm/brightness/ " + valueTemp
				,"!!!DEPRECATED/OBSOLETE!!! /robotCoord/Xvalue/ " + valueMap
				,"!!!DEPRECATED/OBSOLETE!!! /robotCoord/Yvalue/ " + valueMap
				,"/storage/boxPriority/ " + priority
				,"/robot/position/ <0-4>" 
				,""
				,""
				,"MQTT topics client sends commands to:"
				,""
				,"/robotCommand/target/ " + "<BW || DC || BC || DW>"
				,"/robotCommand/boxPriority/ " + priority
		};
		
		entries = new ArrayList(Arrays.asList(entriesS));
		
		 exit.setMinSize(100, 50);
	     listView.getItems().setAll(entries);
	     StackPane logPane = new StackPane();
	     logPane.getChildren().addAll(listView, exit);
	     StackPane pane = new StackPane();
	     pane.getChildren().add(logPane);         

		
		return pane;
	}
}
