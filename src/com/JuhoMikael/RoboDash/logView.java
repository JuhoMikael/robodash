package com.JuhoMikael.RoboDash;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class logView {

public Button close = new Button("close");
	
public StackPane getLog(){
	
	ListView<String> listView = new ListView();
	ArrayList<String> entries = new ArrayList<String>();
    String logLine;
    
    try {
   	   FileReader fr = new FileReader("eventLog.log");
   	   BufferedReader reader = new BufferedReader(fr);
   	   String route1;

   	   while((logLine=reader.readLine())!=null){
   	    entries.add(logLine);
   	    
   	   } 

   	   
   	}
   	   catch (IOException ioe) {
   	   System.err.println(ioe);
   	}        
    
    
     close.setMinSize(100, 50);
     listView.getItems().setAll(entries);
     StackPane logPane = new StackPane();
     logPane.getChildren().addAll(listView,close);
     StackPane pane = new StackPane();
     pane.getChildren().add(logPane);         

	
	return pane;
}
}
